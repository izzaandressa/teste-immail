import Dashboard     from "@material-ui/icons/Dashboard";
import DashboardPage from "views/Dashboard/Dashboard.js";

const dashboardRoutes = [
  {
    path      : "/report",
    name      : "Means Of Death",
    icon      : Dashboard,
    component : DashboardPage,
    layout    : "/arena"
  },
];

export default dashboardRoutes;
