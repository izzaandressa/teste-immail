# teste-immail

![picture alt](https://hb.imgix.net/d2148de45bd17c1e5227822f0d154f5d697d23b2.jpg?auto=compress,format&fit=crop&h=353&w=616&s=8f9925aee7b61698db520edfbb9297bc "Quake III Arena")

## Sobre

Esse projeto tem como finalidade exibir o relatório dos jogos da arena III, consumindo a api [https://gitlab.com/izzaandressa/teste-sp-api-parser](url).

## Para rodar
```
git clone https://gitlab.com/izzaandressa/teste-immail.git

cd teste-immail

npm install

npm start

Espere ou abra http://localhost:3000/
```

## To Do

- [x] Layout responsivo