import React, {useEffect, useState}  from "react";
// react plugin for creating charts
// @material-ui/core
import { makeStyles }  from "@material-ui/core/styles";
// @material-ui/icons
import DonutLarge      from "@material-ui/icons/DonutLarge";
import HourglassEmpty  from "@material-ui/icons/HourglassEmpty";
import Explore         from "@material-ui/icons/Explore";
import Language        from "@material-ui/icons/Language";
import GameIcon        from "@material-ui/icons/Games";
import TrendingUp      from "@material-ui/icons/TrendingUp";

import AlarmOn         from "@material-ui/icons/AlarmOn";
// core components
import GridItem        from "components/Grid/GridItem.js";
import GridContainer   from "components/Grid/GridContainer.js";
import Table           from "components/Table/Table.js";
import CustomTabs      from "components/CustomTabs/CustomTabs.js";
import Card            from "components/Card/Card.js";
import CardHeader      from "components/Card/CardHeader.js";
import CardIcon        from "components/Card/CardIcon.js";
import CardBody        from "components/Card/CardBody.js";
import CardFooter      from "components/Card/CardFooter.js";

import styles          from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import axios           from 'axios';

const useStyles = makeStyles(styles);

export default function Dashboard() {

  const [games, setGames]     = useState([]);
  const [ranking, setRanking] = useState([]);

  useEffect(() => {
      axios.get('http://localhost:4000/games').then((resp) => {
         let _ranking         = {}
         let _rankingByGame   = resp.data
         resp.data.map((_games, keys)=> {
             if (!_games.kills) {
                 _games.players.map((player) => {
                     _games['kills']         = {}
                     _games['kills'][player] = 0
                 })
             }

             Object.keys(_games.kills).map((_player)=>{
                _games.kills[_player] = _games.kills[_player] || 0
                 if (_player in _ranking) {
                     _ranking[_player] += _games.kills[_player]
                 }else{
                     _ranking[_player] = _games.kills[_player]
                 }
             })

             _rankingByGame.push[keys]  = _games
             _rankingByGame[keys].kills = organizeArray(_games.kills)
         })

         setGames(_rankingByGame)
         setRanking(organizeArray(_ranking))
      })
  }, []);

  const organizeArray = _ranking =>{
      let array = [];
      for ( var rank  in _ranking) {
          array.push([rank, _ranking[rank].toString() ])
      }

      const sortedArr = array.sort((a, b) => {
         return parseFloat( parseFloat(b[1] - a[1]))
      })

      sortedArr.map((ranking, key)=>{
          ++key
          ranking.unshift(key.toString())
      })

      return sortedArr
  }

  const classes   = useStyles();

  const TableHead = ["Ranking", "Player", "Kills"]
  const IconColor = ['success', 'warning', 'danger', 'info', 'rose']
  const Icon      = [<DonutLarge />, <HourglassEmpty  />, <Explore  />, <Language  />,  <TrendingUp  />]
  const range     = () => {
      let rand      = Math.random();
      let total     = IconColor.length;
      let randIndex = Math.floor(rand * total)

      return randIndex
  }

  function reportyByGame() {
    return games.map((game, gameIndex) => {
      return (
        <GridItem xs={12} sm={6} md={4} key={gameIndex} >
          <Card >
            <CardHeader color="success" stats icon>
              <CardIcon color={IconColor[range()]}>
                {Icon[range()]}
              </CardIcon>
              <p className={classes.cardCategory}> Total kills | <b>{game.total_kills}</b> </p>
            </CardHeader>
            <CardBody >
              <Table
                tableHeaderColor="warning"
                tableHead       ={TableHead}
                tableData       ={game.kills}
              />
            </CardBody>
            <CardFooter stats>
              <div className={classes.stats}>
                <AlarmOn /> Duration: {game.duration_game}
              </div>
            </CardFooter>
          </Card>
        </GridItem>)
    })
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <CustomTabs
            title=""
            headerColor="primary"
            tabs={[
              {
                tabName   : "Ranking of Kills",
                tabIcon   : TrendingUp,
                tabContent: (
                  <CardBody>
                    <Table
                      tableHeaderColor="warning"
                      tableHead       ={TableHead}
                      tableData       ={ranking}
                    />
                  </CardBody>
                )
              },
              {
                tabName   : "Reporty By Game",
                tabIcon   : GameIcon,
                tabContent: (
                  <GridContainer>
                    {reportyByGame()}
                  </GridContainer>
                )
              },
            ]}
          />
        </GridItem>
      </GridContainer>
    </div>
  );
}
